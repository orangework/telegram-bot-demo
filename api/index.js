const express = require('express')
const app = express()
const PORT = 3001

const db = require('./src/config/db')
const Record = require('./src/models/Record')

const MESSAGE_START = 'START';
const MESSAGE_DEFAULT = 'DEFAULT';

app.get('/start', async (req, res) => {
    const message = await Record.findOne({ where: { type: MESSAGE_START } })
    res.send(message.dataValues.text)
})

app.get('/default', async (req, res) => {
    const message = await Record.findOne({ where: { type: MESSAGE_DEFAULT } })
    res.send(message.dataValues.text)
})

app.listen(PORT, async () => {
    await db.sync()
    const records = await Record.findAll()
    
    if(records.length !== 2){
        const startRecord = await Record.create({ text: "Welcome", type: MESSAGE_START });
        const defaultRecord = await Record.create({ text: "Default", type: MESSAGE_DEFAULT });

        await startRecord.save()
        await defaultRecord.save()
    }

    console.log(`Listen :${PORT}`);
})
