const { Sequelize } = require('sequelize');
const path = require('path');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: `${path.resolve('.')}/db/database.sqlite`
});

module.exports = sequelize;