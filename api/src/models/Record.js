const { DataTypes } = require('sequelize')
const db = require('../config/db')

const Record = db.define('Record', {
    text: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    }
  },
  {}
);

module.exports = Record;