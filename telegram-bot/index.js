const axios = require('axios')
const { Telegraf } = require('telegraf')

const TOKEN = "1609476591:AAGdSBgF3I5Ku1pq_fKGJgyHOiIa3EQpFuI"
const { API_HOST = 'localhost:3001' } = process.env

const bot = new Telegraf(TOKEN)

bot.start(async (ctx) => {
    try{
        const { data: serverMessage } = await axios.get(`http://${API_HOST}/start`)
        ctx.reply(serverMessage)
    }
    catch(e){
        console.log(e)
    }
})

bot.on('text', async (ctx) => {
    try{
        const { data: serverMessage } = await axios.get(`http://${API_HOST}/default`)
        ctx.reply(serverMessage)
    }
    catch(e){
        console.log(e)
    }
    
})

bot.launch()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))